#pragma once

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/icmp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <cstdint>
#include <forward_list>
#include <random>
#include <sstream>
#include <string>
#include <string_view>

#include "icmp_header.hpp"
#include "ip_header.hpp"

class lag final
{
public:
    using ping_cb =
        std::function<void(const boost::asio::ip::address &dst, uint64_t nano)>;

    using ping_cb_timeout =
        std::function<void(const boost::asio::ip::address &dst)>;

    lag(boost::asio::io_context &ctx, std::string_view dst) :
        m_ctx(ctx),
        m_rslv(ctx),
        m_dst(dst)
    {
    }

    void
    async_ping(ping_cb         f = { [](auto &&, auto &&) {} },
               ping_cb_timeout g = { [](auto &&) {} })
    {
        m_rslv.async_resolve(m_dst, {}, [this, f, g](auto &&err, auto &&range) {
            for (auto &&point : range) {
                m_inst.emplace_front(m_ctx, point, f, g);
            }
        });
    }

private:
    struct instance;
    boost::asio::io_context        &m_ctx;
    boost::asio::ip::icmp::resolver m_rslv;
    std::forward_list<instance>     m_inst;
    std::string_view                m_dst;
    friend std::ostream &
    operator<<(std::ostream &out, const lag &l);
};

struct lag::instance
{
    uint8_t                         m_replies {};
    uint8_t                         m_timeout {};
    double                          m_time {};
    uint8_t                         m_seq {};
    uint8_t                         m_last_seq {};
    boost::asio::ip::icmp::socket   m_socket;
    boost::asio::ip::icmp::endpoint m_dst;
    boost::asio::steady_timer       m_timer;
    const uint16_t                  m_id;
    boost::asio::streambuf          m_recv_buff;
    boost::asio::streambuf          m_snd_buff;
    lag::ping_cb                    m_ping_ok;
    lag::ping_cb_timeout            m_ping_t;

    static constexpr uint8_t timeout_limit { 5 };
    static constexpr uint8_t replies_limit { 5 };

    static uint16_t
    gen_id()
    {
        std::random_device rnd;
        std::mt19937       mt(rnd());
        return mt();
    }

    icmp_header::type
    get_request_version() const
    {
        return m_dst.address().is_v4() ? icmp_header::type::v4_echo_request
                                       : icmp_header::type::v6_echo_request;
    }
    icmp_header::type
    get_reply_version() const
    {
        return m_dst.address().is_v4() ? icmp_header::type::v4_echo_reply
                                       : icmp_header::type::v6_echo_reply;
    }

    instance(boost::asio::io_context        &ctx,
             boost::asio::ip::icmp::endpoint dst,
             lag::ping_cb                    cb,
             lag::ping_cb_timeout            cb_out) :
        m_socket(ctx,
                 dst.address().is_v4() ? boost::asio::ip::icmp::v4()
                                       : boost::asio::ip::icmp::v6()),
        m_dst(dst),
        m_timer(ctx),
        m_id(gen_id()),
        m_ping_ok(cb),
        m_ping_t(cb_out)

    {
        recv();
        send();
    }

    void
    send()
    {
        m_snd_buff.consume(m_snd_buff.size());
        std::ostream os(&m_snd_buff);

        icmp_header header;
        header.set_id(m_id);
        header.set_type(get_request_version());
        header.set_seq(++m_seq);

        std::stringstream ss;

        // get timestamp
        std::chrono::steady_clock sc;
        ss << sc.now().time_since_epoch().count();

        auto now_str(ss.str());

        header.compute_checksum(header, now_str.begin(), now_str.end());

        os << header << now_str;

        m_socket.async_send_to(m_snd_buff.data(),
                               m_dst,
                               std::bind(&lag::instance::did_send, this));
    }

    void
    did_send()
    {
        // init timeout timer
        m_timer.expires_from_now(std::chrono::seconds(1));
        m_timer.async_wait(std::bind(&lag::instance::do_timeout, this));
    }

    void
    do_timeout()
    {
        if (m_last_seq + 1 <= m_seq) {
            m_ping_t(m_dst.address());
        }
        if (++m_timeout < timeout_limit && m_replies < replies_limit) {
            m_timer.expires_from_now(std::chrono::seconds(1));
            m_timer.async_wait(std::bind(&lag::instance::send, this));
        }
    }

    void
    recv()
    {
        m_recv_buff.consume(m_recv_buff.size());
        m_socket.async_receive(
            m_recv_buff.prepare(1500),
            std::bind(&lag::instance::do_recv, this, std::placeholders::_2));
    }

    void
    do_recv(std::size_t len)
    {
        auto now = std::chrono::steady_clock::now().time_since_epoch().count();

        m_recv_buff.commit(len);
        std::istream in(&m_recv_buff);

        if (!in) {
            return;
        }

        icmp_header icmph;

        if (m_dst.address().is_v4()) {
            ipv4_header i4h;
            in >> i4h;
        }

        in >> icmph;

        // check if matching what we sent
        if (in && icmph.get_id() == m_id &&
            icmph.get_type() == get_reply_version() &&
            icmph.get_seq() == m_seq) {
            m_replies++;

            m_last_seq = icmph.get_seq();

            // get timestamp
            uint64_t orig_time;
            in >> orig_time;
            if (in) {
                m_time += (now - orig_time);
                m_ping_ok(m_dst.address(), now - orig_time);
            }
        }

        if (m_timeout < timeout_limit && m_replies < replies_limit)
            recv();
    }
};

inline std::ostream &
operator<<(std::ostream &out, const lag &l)
{
    out << boost::format("%1% %|39t|%2%\n") % "target" % "lag in seconds";
    for (auto &&inst : l.m_inst) {
        auto fmt = boost::format("%1% %|39t|%2%\n");
        fmt % inst.m_dst.address();
        if (inst.m_replies < inst.replies_limit) {
            fmt % "t'out";
        } else {
            fmt % ((inst.m_time / double(inst.m_replies)) / pow(10, 9));
        }
        out << fmt;
    }

    return out;
}
