#include <boost/asio/io_context.hpp>
#include <boost/format.hpp> // somehow std::format is missing :(
#include <cstddef>
#include <iostream>

#include "icmp_lag.hpp"

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "pass a destination host\n";
        return EXIT_FAILURE;
    }

    try {
        boost::asio::io_context ctx;
        lag                     l(ctx, argv[1]);

        l.async_ping(
            [](auto &&dst, auto &&nano) {
                std::cout << boost::format("%1% %|39t|%2%\n") % dst %
                                 (nano / pow(10, 9));
            },
            [](auto &&dst) {
                std::cout << boost::format("%1% %|39t|%2%\n") % dst % "t'out";
            });

        ctx.run();

        std::cout << l << "\n";

        return EXIT_SUCCESS;
    } catch (const std::exception &ex) {
        std::cerr << ex.what() << "\n";
        return EXIT_FAILURE;
    }
}
