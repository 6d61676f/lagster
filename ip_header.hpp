#pragma once

#include <array>
#include <cstdint>
#include <istream>
#include <ostream>
#include <tuple>
#include <variant>

class ip_header_common
{
protected:
    std::array<uint8_t, 60> m_repr { 0 }; // v4 is max 60, v6 is fixed 40

public:
    static uint16_t
    decode(uint8_t a, uint8_t b)
    {
        return uint16_t(a) << 8 | b;
    }

    static std::tuple<uint8_t, uint8_t>
    encode(uint16_t no)
    {
        return { no >> 8 & 0xFF, no & 0xFF };
    }

    static uint32_t
    decode(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
    {
        return uint32_t(a) << 24 | uint32_t(b) << 16 | uint32_t(c) << 8 | d;
    }

    virtual uint8_t
    get_version() const
    {
        return m_repr[0] >> 4;
    }

    virtual uint8_t
    get_header_size() const = 0;

    virtual uint8_t
    get_protocol() const = 0;

    virtual uint8_t
    get_ttl() const = 0;
};

class ipv4_header : public ip_header_common
{
public:
    void
    set_version(uint8_t v)
    {
        m_repr[0] = m_repr[0] | ((v & 0xF) << 4);
    }

    uint8_t
    get_header_size() const override
    {
        return (m_repr[0] & 0xF) * 4;
    }

    void
    set_header_size(uint8_t sz)
    {
        m_repr[0] = m_repr[0] | ((sz / 4) & 0xF);
    }

    uint16_t
    get_total_size() const
    {
        return decode(m_repr[2], m_repr[3]);
    }

    void
    set_total_size(uint16_t sz)
    {
        auto &&[a, b] = encode(sz);
        m_repr[2]     = a;
        m_repr[3]     = b;
    }

    uint8_t
    get_protocol() const override
    {
        return m_repr[9];
    }

    uint8_t
    get_ttl() const override
    {
        return m_repr[8];
    }

    void
    set_ttl(uint8_t v)
    {
        m_repr[8] = v;
    }

    void
    set_protocol(uint8_t proto)
    {
        m_repr[9] = proto;
    }

    uint32_t
    get_src_addr() const
    {
        return decode(m_repr[12], m_repr[13], m_repr[14], m_repr[15]);
    }

    uint32_t
    get_dst_addr() const
    {
        return decode(m_repr[16], m_repr[17], m_repr[18], m_repr[19]);
    }

    friend std::istream &
    operator>>(std::istream &in, ipv4_header &v4)
    {
        in.read(reinterpret_cast<char *>(v4.m_repr.data()), 20);
        auto optlen = short(v4.get_header_size()) - 20;
        if (optlen < 0 || optlen > 40) {
            in.setstate(std::ios::failbit);
        } else {
            in.read(reinterpret_cast<char *>(v4.m_repr.data() + 20), optlen);
        }

        return in;
    }
};

class ipv6_header : public ip_header_common
{
public:
    uint8_t
    get_header_size() const override
    {
        return 40;
    }

    uint8_t
    get_protocol() const override
    {
        return m_repr[6];
    }

    uint8_t
    get_ttl() const override
    {
        return m_repr[7];
    }

    friend std::istream &
    operator>>(std::istream &in, ipv6_header &v6)
    {
        in.read(reinterpret_cast<char *>(v6.m_repr.data()), 40);
        return in;
    }
};

class ip_header : public ip_header_common
{
    uint8_t                                m_type {};
    std::variant<ipv4_header, ipv6_header> m_inner {};

public:
    uint8_t
    get_header_size() const override
    {
        return std::visit(
            [](auto &&v) {
                return v.get_header_size();
            },
            m_inner);
    }

    uint8_t
    get_protocol() const override
    {
        return std::visit(
            [](auto &&v) {
                return v.get_protocol();
            },
            m_inner);
    }

    uint8_t
    get_ttl() const override
    {
        return std::visit(
            [](auto &&v) {
                return v.get_ttl();
            },
            m_inner);
    }

    friend std::istream &
    operator>>(std::istream &in, ip_header &hdr)
    {
        hdr.m_type = in.peek() >> 4;

        if (hdr.m_type == 4) {
            ipv4_header v4;
            in >> v4;
            hdr.m_inner = v4;
        } else if (hdr.m_type == 6) {
            ipv6_header v6;
            in >> v6;
            hdr.m_inner = v6;
        } else {
            in.setstate(std::ios::failbit);
        }
        return in;
    }
};
