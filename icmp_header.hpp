#pragma once

#include <array>
#include <bit>
#include <cstddef>
#include <cstdint>
#include <istream>
#include <tuple>

// TODO there's also ICMPv6

class icmp_header
{
private:
    std::array<uint8_t, 8> m_repr { 0 }; // 8 for standard header

    static uint16_t
    decode(uint16_t a, uint16_t b)
    {
        return a << 8 | b;
    }

    static std::tuple<uint8_t, uint8_t>
    encode(uint16_t val)
    {
        return { val >> 8 & 0xFF, val & 0xFF };
    }

public:
    template<typename Iter>
    static void
    compute_checksum(icmp_header &hdr, Iter begin, Iter end)
    {
        uint32_t sum = (hdr.get_type() << 8) + hdr.get_code() + hdr.get_id() +
                       hdr.get_seq();

        while (begin != end) {
            sum += static_cast<uint8_t>(*begin++) << 8;
            if (begin != end) {
                sum += static_cast<uint8_t>(*begin++);
            }
        }

        sum = (sum >> 16) + (sum & 0xFFFF);
        sum += (sum >> 16);
        hdr.set_checksum(~sum);
    }

    enum type : uint8_t
    {
        v4_echo_reply   = 0,
        v4_echo_request = 8,
        v6_echo_request = 128,
        v6_echo_reply   = 129,
    };

    uint8_t
    get_type() const
    {
        return m_repr[0];
    }

    void
    set_type(icmp_header::type t)
    {
        m_repr[0] = t;
    }

    uint8_t
    get_code() const
    {
        return m_repr[1];
    }

    uint16_t
    get_id() const
    {
        return decode(m_repr[4], m_repr[5]);
    }

    void
    set_id(uint16_t id)
    {
        auto [a, b] = encode(id);
        m_repr[4]   = a;
        m_repr[5]   = b;
    }

    uint16_t
    get_seq() const
    {
        return decode(m_repr[6], m_repr[7]);
    }

    void
    set_seq(uint16_t v)
    {
        auto [a, b] = encode(v);
        m_repr[6]   = a;
        m_repr[7]   = b;
    }

    void
    set_checksum(uint16_t val)
    {
        auto [a, b] = encode(val);
        m_repr[2]   = a;
        m_repr[3]   = b;
    }

    friend std::istream &
    operator>>(std::istream &in, icmp_header &icmp)
    {
        in.read(reinterpret_cast<char *>(icmp.m_repr.data()),
                icmp.m_repr.size());
        return in;
    }

    friend std::ostream &
    operator<<(std::ostream &out, icmp_header &icmp)
    {
        out.write(reinterpret_cast<char *>(icmp.m_repr.data()),
                  icmp.m_repr.size());
        return out;
    }
};
